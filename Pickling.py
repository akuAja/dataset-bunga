import numpy as np
import pickle
import glob
import cv2


train_data = []
label_data = []

for x in glob.glob("D:\\Bunga\\daisy\\*.jpg"):
    img = cv2.cvtColor(cv2.imread(x),cv2.COLOR_BGR2RGB)
    resized = cv2.resize(img, (224,224), interpolation = cv2.INTER_AREA)
    train_data.append((resized))
    label_data.append(0)

for x in glob.glob("D:\\Bunga\\dandelion\\*.jpg"):
    img = cv2.cvtColor(cv2.imread(x),cv2.COLOR_BGR2RGB)
    resized = cv2.resize(img, (224,224), interpolation = cv2.INTER_AREA)
    train_data.append((resized))
    label_data.append(1)

for x in glob.glob("D:\\Bunga\\rose\\*.jpg"):
    img = cv2.cvtColor(cv2.imread(x),cv2.COLOR_BGR2RGB)
    resized = cv2.resize(img, (224,224), interpolation = cv2.INTER_AREA)
    train_data.append((resized))
    label_data.append(2)

for x in glob.glob("D:\\Bunga\\sunflower\\*.jpg"):
    img = cv2.cvtColor(cv2.imread(x),cv2.COLOR_BGR2RGB)
    resized = cv2.resize(img, (224,224), interpolation = cv2.INTER_AREA)
    train_data.append((resized))
    label_data.append(3)
    
for x in glob.glob("D:\\Bunga\\tulip\\*.jpg"):
    img = cv2.cvtColor(cv2.imread(x),cv2.COLOR_BGR2RGB)
    resized = cv2.resize(img, (224,224), interpolation = cv2.INTER_AREA)
    train_data.append((resized))
    label_data.append(4)

data_bunga = {"img_data" : np.array(train_data), "label_data" : np.array(label_data)}
print(data_bunga["img_data"].shape)
output = open('D:\\Bunga\\DatasetBunga.p','wb')
pickle.dump(data_bunga,output)
output.close()

csvWrite = open('D:\\Bunga\\DatasetBunga.csv','w')
columnTitleRow = "ClassID,Class\n"
csvWrite.write(columnTitleRow)
csvWrite.write("0,DAISY\n")
csvWrite.write("1,DANDELION\n")
csvWrite.write("2,ROSE\n")
csvWrite.write("3,SUNFLOWER\n")
csvWrite.write("4,TULIP\n")
csvWrite.close()
